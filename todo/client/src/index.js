import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import TodoContainer from "./components/Todo/todo";
import AuthComponent from "./components/AuthModule/AuthComponent";
function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/auth">
            <AuthComponent />
          </Route>
          <Route path="/todos">
            <TodoContainer />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
