import React from "react";
import TodoListItem from "./TodoListItem";

import "./TodoList.css";

class TodoList extends React.Component {
  constructor() {
    super();
    this.state = {
      todos: null,
    };
  }

  elements = () => {
    const { todos } = this.state;
    const { onInportantClick, onDeleteClick, onDoneClick } = this.props;

    const todoElements = todos.map((item) => {
      const { id, ...itemProps } = item;
      return (
        <li key={id} className="list-group-item">
          <TodoListItem
            {...itemProps}
            id={id}
            onInportantClick={onInportantClick}
            onDeleteClick={onDeleteClick}
            onDoneClick={onDoneClick}
          />
        </li>
      );
    });
    return todoElements;
  };

  componentWillReceiveProps() {
    const { todos } = this.props;
    this.setState({ todos: todos });
  }

  render() {
    const { todos } = this.state;
    return (
      <div>
        <ul className="list-group todo-list">{todos ? this.elements() : ""}</ul>
      </div>
    );
  }
}

export default TodoList;
