import React from "react";

import "./TodoListItem.css";

class TodoListItem extends React.Component {
  constructor() {
    super();
    this.state = {
      isDone: 0,
      isActive: 0,
    };
  }

  onImportantButton = (id) => {
    const { onInportantClick } = this.props;
    if (this.state.isActive) {
      this.setState({ isActive: 0 });
      return onInportantClick(id, { is_active: 1 });
    } else {
      this.setState({ isActive: 1 });
      return onInportantClick(id, { is_active: 1 });
    }
  };

  onDeleteButton = (id) => {
    const { onDeleteClick } = this.props;
    return onDeleteClick(id);
  };

  onDone = (id) => {
    const { onDoneClick } = this.props;
    if (this.state.isActive) {
      this.setState({ isDone: 0 });
      return onDoneClick(id, { is_done: 1 });
    } else {
      this.setState({ isDone: 1 });
      return onDoneClick(id, { is_done: 1 });
    }
  };

  render() {
    const { name, id, is_done, is_active } = this.props;
    const { isActive, isDone } = this.state;
    let classNames = ["todo-list-item"];

    if (isDone || is_done) classNames.push("done");

    if (isActive || is_active) classNames.push("important");
    return (
      <span className={classNames.join(" ")}>
        <span className="todo-list-item-label" onClick={() => this.onDone(id)}>
          {name}
        </span>

        <button
          type="button"
          className="btn btn-outline-success btn-sm float-right"
          onClick={() => this.onImportantButton(id)}
        >
          <i className="fa fa-exclamation" />
        </button>

        <button
          type="button"
          className="btn btn-outline-danger btn-sm float-right"
          onClick={() => this.onDeleteButton(id)}
        >
          <i className="fa fa-trash-o" />
        </button>
      </span>
    );
  }
}

export default TodoListItem;
