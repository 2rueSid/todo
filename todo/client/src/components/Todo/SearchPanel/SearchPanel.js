import React from 'react';

import './SearchPanel.css';

class SearchPanel extends React.Component {
	onLabelChange = (e) => {
		this.props.onSearchItem(e.target.value);
	};

	render() {
		return (
			<input
				type='text'
				className='form-control search-input'
				placeholder='type to search'
				onChange={this.onLabelChange}
			/>
		);
	}
}

export default SearchPanel;