import React from "react";
import withNetworking from "@hocs/withNetworking";

class AddItem extends React.Component {
  state = {
    label: "",
  };
 
  addTodo = async (task) => {
    const { httpClient } = this.props;

	await httpClient
      .post("/todos", {name: task})
      .then((res) => {
        if (res.status === 200) {
          console.log(res.data.message);
        }
      })
      .catch((e) => {
        if (e) {
          console.log(e);
        }
      });
  };

  onLabelChange = (e) => {
    this.setState({ label: e.target.value });
  };

  onSubmit = async (e) => {
	const { label } = this.state;
    e.preventDefault();
	await this.addTodo(label)
    this.setState({ label: "" });
  };
  render() {
    return (
      <form className="item-add-form d-flex" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="add new task"
          className="form-control"
          onChange={this.onLabelChange}
          value={this.state.label}
        ></input>
        <button className="btn btn-outline-secondary">Add task</button>
      </form>
    );
  }
}

export default withNetworking(AddItem);
