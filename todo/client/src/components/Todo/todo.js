import React from "react";

import AppHeader from "./AppHeader/AppHeader";
import SearchPanel from "./SearchPanel/SearchPanel";
import TodoList from "./ToDoList/TodoList";
import ItemStatusFilter from "./ItemStatusFilter/ItemStatusFilter";
import AddNewTask from "./AddItem/AddItem";
import withNetworking from "@hocs/withNetworking";
import "./todo.css";

class TodoContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      todos: null,
    };
  }
  getTodos = async () => {
    const { httpClient } = this.props;

    await httpClient
      .get("/todos")
      .then((res) => {
        this.setState({ todos: Array.from(res.data.todos) });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  updateTodo = async (data) => {
    const { httpClient } = this.props;

    await httpClient
      .put("/todos", data)
      .then((res) => {
        console.log(res.message);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  deleteTodo = async (id) => {
    const { httpClient } = this.props;

    await httpClient
      .delete("/todos", { data: { todo_id: id } })
      .then((res) => {
        console.log(res.message);
        
      })
      .catch((e) => {
        console.log(e);
      });
  };

  async componentDidMount() {
    await this.getTodos();
  }
  async componentWillMount() {
    await this.getTodos();
  }

  onDeleteClick = async (id) => {
    await this.deleteTodo(id);
  };
  onInportantClick = async (id, data) => {
    await this.updateTodo({ data, id });
  };
  onDoneClick = async (id, data) => {
    await this.updateTodo({ data, id });
  };
  render() {
    const { todos } = this.state;
    return (
      <div className="todo-app">
        <AppHeader />
        <div className="top-panel d-flex">
          <SearchPanel />
          <ItemStatusFilter />
        </div>
        <TodoList
          todos={todos}
          onInportantClick={this.onInportantClick}
          onDeleteClick={this.onDeleteClick}
          onDoneClick={this.onDoneClick}
        />
        <AddNewTask />
      </div>
    );
  }
}

export default withNetworking(TodoContainer);
