import React from 'react';

import './ItemStatusFilter.css';

class ItemStatusFilter extends React.Component {
	render() {
		const { onAll, onDone, onActive } = this.props;
		return (
			<div className='btn-group'>
				<button type='button' className='btn btn-info' onClick={onAll}>
					All
				</button>
				<button type='button' className='btn btn-outline-secondary' onClick={onActive}>
					Active
				</button>
				<button type='button' className='btn btn-outline-secondary' onClick={onDone}>
					Done
				</button>
			</div>
		);
	}
}

export default ItemStatusFilter;