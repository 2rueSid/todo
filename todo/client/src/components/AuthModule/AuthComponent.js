import React from "react";
import { Icon } from "@material-ui/core";
import AuthFormBox from "./AuthMain/FormBox/FormBox";

import "./authComponent.css";
function AuthComponent() {
  return (
    <div className="auth-component">
      <div className="auth-side">
        <Icon className="icon" fontSize={"large"}>
          <i className="fas fa-seedling"></i>
        </Icon>
      </div>
      <AuthFormBox />
    </div>
  );
}

export default AuthComponent;
