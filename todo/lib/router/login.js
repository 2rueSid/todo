const login = require("express").Router();
const loginController = require("../controllers/loginController");
const loginMiddleware = require("../middlewares/login");

login.post("/", loginMiddleware.loginValidate, loginController.login);

module.exports = login;
