const todos = require("express").Router();
const passport = require("passport");
const todosController = require("../controllers/todosController");
todos.use(passport.authenticate("jwt", { session: false }));

todos.post("/", todosController.createNewTask);

todos.get("/", todosController.getTasks);

todos.put("/", todosController.updateTask);

todos.delete("/", todosController.deleteTask);

module.exports = todos;
