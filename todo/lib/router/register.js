const register = require("express").Router();
const registerController = require("../controllers/regsiterController");
const registerMiddleware = require("../middlewares/register");

register.post(
  "/",
  registerMiddleware.addUserValidate,
  registerController.addUser
);

module.exports = register;
