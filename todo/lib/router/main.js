const loginRout = require("./login");
const registerRout = require("./register");
const todosRout = require("./todos");

const setupRoutes = (app) => {
  app.use("/auth/register", registerRout);
  app.use("/auth/login", loginRout);
  app.use("/todos", todosRout);
};

module.exports = setupRoutes;
