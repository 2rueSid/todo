const { knex } = require("../../knexfile");

class BaseModel {
  constructor(tableName) {
    this.tableName = tableName;
  }

  async insertData(data) {
    if (typeof data === "object") {
      await knex(this.tableName)
        .insert(data)
        .then(() => "data is inserted")
        .catch((e) => {
          console.log(e);
        });
      return true;
    }
    return false;
  }

  async selectData(whereCondition = {}, whatNeedToSelect = ["*"]) {
    let foundData = null;
    await knex
      .from(this.tableName)
      .select(whatNeedToSelect)
      .where(whereCondition)
      .then((rows) => {
        foundData = rows;
      })
      .catch((e) => {
        console.log(e);
      });

    return !foundData.length ? null : foundData;
  }

  async updateData(whereCondition = {}, data) {
    if (typeof data === "object") {
      await knex(this.tableName)
        .where(whereCondition)
        .update(data)
        .then(() => "data is updated")
        .catch((e) => {
          console.log(e);
        });
      return true;
    }
    return false;
  }

  async deleteData(whereCondition = {}) {
    await knex(this.tableName)
      .where(whereCondition)
      .del()
      .then(() => "data is deleted")
      .catch((e) => {
        console.log(e);
      });
    return true;
  }
}
module.exports = BaseModel;
