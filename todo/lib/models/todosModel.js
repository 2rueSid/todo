const BaseModel = require("./baseModel");

class Todos extends BaseModel {
  constructor() {
    super("todos");
  }

  async createTodo(todoData) {
    const data = {
      name: todoData.name,
      user_id: todoData.userId,
    };
    const query = await this.insertData(data);
    return query;
  }

  async selectTodo(userId, whatNeedToSelect = ["*"]) {
    const whereCondition = { user_id: userId };
    const query = await this.selectData(whereCondition, whatNeedToSelect);
    
    return query;
  }

  async updateTodo(todo_id, whatNeedToUpdate) {
    const query = await this.updateData({id: todo_id }, whatNeedToUpdate);
    return query;
  }

  async deleteTodo(todo_id) {
    const whereCondition = { id: todo_id };

    const query = await this.deleteData(whereCondition);
    return query;
  }

 
}

module.exports = new Todos();
