module.exports.up = function createUserTable(knex) {
  return knex.schema
    .createTable("todos", (table) => {
      table.increments().unsigned().primary();
      table.string("name").notNull();
      table.integer("user_id").notNullable();
      table.integer("is_active").defaultTo(0);
      table.integer("is_done").defaultTo(0);
      table.dateTime("created_at", { precision: 6 }).defaultTo(knex.fn.now(6));
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .then(() => "table todos is created")
    .catch((err) => {
      throw err;
    })
    .finally(() => knex.destroy());
};

module.exports.down = function dropUsersTable(knex) {
  return knex.schema.dropTable("todos").catch((err) => {
    throw err;
  });
};
