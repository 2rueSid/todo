const todos = require("../todos");

exports.up = async function (knex) {
  await todos.up(knex);
};

exports.down = async function (knex) {
  await todos.down(knex);
};
