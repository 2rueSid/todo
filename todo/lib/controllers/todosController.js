const Todo = require("../models/todosModel");

module.exports = {
  createNewTask:
    ("/",
    async (req, res) => {
      try {
        const newTodo = req.body;
        const { user } = req;
        if (user) {
          await Todo.createTodo({ name: newTodo.name, userId: user.id });
          res.status(200).json({ message: "todo is succesfully created" });
        } else {
          res.status(502).json({ message: "server error" });
        }
      } catch (e) {
        res.status(502).json({ message: "server error" });
      }
    }),

  getTasks:
    ("/",
    async (req, res) => {
      try {
        const { user } = req;

        if (user) {
          const userTodos = await Todo.selectTodo(user.id);

          res.status(200).json({ todos: userTodos });
        } else {
          res.status(502).json({ message: "server error" });
        }
      } catch (e) {
        res.status(502).json({ message: "server error" });
      }
    }),

  updateTask:
    ("/",
    async (req, res) => {
      const { user } = req;
      const updatedTodo = req.body;
      if (user) {
        await Todo.updateTodo(updatedTodo.id, updatedTodo.data);

        res.status(200).json({ message: "successfull" });
      } else {
        res.status(502).json({ message: "server error" });
      }
    }),

  deleteTask:
    ("/",
    async (req, res) => {
      try {
        const { user } = req;
        const task = req.body;
        if (user) {
          const newTodos = await Todo.deleteTodo(task.todo_id);

          res.status(200).json({ message: "successfull", todos: newTodos });
        } else {
          res.status(502).json({ message: "server error" });
        }
      } catch (e) {
        res.status(502).json({ message: "server error" });
      }
    }),
};
