const User = require("../models/usersModel");

module.exports = {
  addUser: async (req, res) => {
    try {
      if (req.validateErrors.length) {
        res.status(400).json(req.validateErrors);
      } else {
        const user = req.body;
        const findUser = await User.selectUser(user.email);

        if (!findUser) {
          user.password = await User.hashUserPassword(user.password, 10);
          await User.createUser(user);

          res.status(200).json({
            message: "new user is created",
            user: user,
          });
        } else {
          res
            .status(400)
            .json({ message: "user with that email is allready exist" });
        }
      }
    } catch (e) {
      res.status(502).json({ message: "server error" });
    }
  },
};
