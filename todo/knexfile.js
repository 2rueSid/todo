const config = {
  development: {
    client: "mysql2",
    connection: {
      host: "127.0.0.1",
      user: "root",
      password: "13372828",
      database: "todo_app_cursova",
    },
    migrations: {
      directory: "./lib/database/migration",
    },
  },
};
module.exports = config;

module.exports.knex = require("knex")(config.development);
